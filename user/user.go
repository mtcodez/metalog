package user

import (
	"code.google.com/p/go.crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type User struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Username string
	Password []byte
}

func (u *User) SetPassword(password string) {
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	u.Password = pass
}
