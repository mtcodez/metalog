package main

import (
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/julienschmidt/httprouter"
	"github.com/russross/blackfriday"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"log"
	"metacodez.com/mtlog/user"
	"net/http"
)

type Article struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Title    string
	Author   string
	Contents template.HTML
}

func (a *Article) ReturnSubWords() template.HTML {
	if len(a.Contents) < 300 {
		return a.Contents
	} else {
		return a.Contents[:300]
	}
}

var (
	store   sessions.Store
	session *mgo.Session
)

func main() {
	router := httprouter.New()

	session = ConnectToDB()
	defer session.Close()

	store = sessions.NewCookieStore(securecookie.GenerateRandomKey(64))
	//	store = sessions.NewCookieStore([]byte("auth-session"))
	//InsertUser(session, "mtcodez@gmail.com", "123456")

	router.GET("/", GetIndex)
	router.GET("/login", GetLogin)
	router.POST("/login", PostLogin)
	router.POST("/logout", PostLogout)
	router.GET("/admin", GetAdmin)
	router.POST("/new_article", PostNewArticle)
	router.GET("/article/:id", GetArticle)

	log.Println("Parsed the templates.")
	router.ServeFiles("/static/css/*filepath", http.Dir("./html/static/css"))
	router.ServeFiles("/static/js/*filepath", http.Dir("./html/static/js"))
	router.ServeFiles("/static/fonts/*filepath", http.Dir("./html/static/fonts"))
	router.ServeFiles("/static/codemirror/*filepath", http.Dir("./html/static/codemirror"))

	//	router.GET("/login", func() {})
	http.ListenAndServe(":8080", router)
}

func ConnectToDB() *mgo.Session {
	session, err := mgo.Dial("localhost")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to DB")
	return session
}
func RetrieveUser(session *mgo.Session, username string, password string) (*user.User, error) {
	c := session.DB("test").C("users")

	var result user.User
	err := c.Find(bson.M{"username": username}).One(&result)
	if err != nil {
		//		log.Fatal(err)
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword(result.Password, []byte(password))
	if err != nil {
		//		log.Fatal(err)
		return nil, err
	}
	return &result, err
}
func InsertUser(session *mgo.Session, username string, password string) error {
	c := session.DB("test").C("users")

	u := &user.User{Username: username}
	u.SetPassword(password)

	err := c.Insert(u)
	if err != nil {
		log.Fatal(err)
	}
	return err
}
func RetrieveArticle(session *mgo.Session) *[]Article {
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	c := session.DB("test").C("articles")

	var result []Article
	err := c.Find(nil).All(&result)
	if err != nil {
		log.Fatal(err)
	}
	return &result
}

func PostLogin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	sess, _ := store.Get(r, "auth")
	if sess.Values["user"] != nil {
		log.Println("Already authenticated.")
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	username, password := r.FormValue("username"), r.FormValue("password")
	//		session.DB("test").C("users").Find(arg0)
	_, err := RetrieveUser(session, username, password)
	if err == nil {
		sess.Values["user"] = username
		sess.Save(r, w)
		log.Println("User logged in.")
		http.Redirect(w, r, "/", http.StatusFound)
		return
	} else {
		log.Println("Username or password is wrong.")
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
}

func GetLogin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	t, err := template.ParseFiles("html/base.tmpl", "html/nav.tmpl", "html/login.tmpl")
	if err != nil {
		log.Fatal(err)
	}
	username, _ := store.Get(r, "auth")
	data := &Data{
		RetrieveArticle(session),
		username.Values["user"],
	}
	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Fatal(err)
	}
}
func GetIndex(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	t, err := template.New("").ParseFiles("html/base.tmpl", "html/nav.tmpl", "html/index.tmpl")
	if err != nil {
		log.Fatal(err)
	}
	username, _ := store.Get(r, "auth")
	data := &Data{
		RetrieveArticle(session),
		username.Values["user"],
	}
	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Fatal(err)
	}
}
func PostLogout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	s, _ := store.Get(r, "auth")
	s.Options.MaxAge = -1
	s.Save(r, w)
	http.Redirect(w, r, "/", http.StatusFound)
	return
}
func GetAdmin(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	t, err := template.New("").ParseFiles("html/base.tmpl", "html/nav.tmpl", "html/edit.tmpl")
	if err != nil {
		log.Fatal(err)
	}
	username, _ := store.Get(r, "auth")
	data := &Data{
		RetrieveArticle(session),
		username.Values["user"],
	}
	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Fatal(err)
	}
}
func PostNewArticle(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	s, _ := store.Get(r, "auth")
	name := s.Values["user"]
	title, contents := r.FormValue("title"), r.FormValue("contents")

	c := session.DB("test").C("articles")
	err := c.Insert(&Article{Title: title, Author: name.(string), Contents: template.HTML(contents)})
	if err != nil {
		log.Fatal(err)
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
func GetArticle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	log.Println(id)
	c := session.DB("test").C("articles")
	var result Article
	err := c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&result)
	if err != nil {
		log.Println(err)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	t, err := template.ParseFiles("html/base.tmpl", "html/nav.tmpl", "html/article.tmpl")
	if err != nil {
		log.Fatal(err)
	}
	username, _ := store.Get(r, "auth")
	data := &DataSingle{
		&result,
		username.Values["user"],
	}

	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Fatal(err)
	}
}

type DataSingle struct {
	Article *Article
	logged  interface{}
	Message string
}

func (d *DataSingle) Logged() bool {
	if d.logged == nil {
		return false
	} else {
		return true
	}
}

type Data struct {
	Articles *[]Article
	logged   interface{}
	Message  string
}

func (d *Data) Logged() bool {
	if d.logged == nil {
		return false
	} else {
		return true
	}
}
